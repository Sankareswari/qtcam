#ifndef WPACLI_H
#define WPACLI_H


#include "os.h"
#define CONFIG_CTRL_IFACE_UNIX
#define CTRL_IFACE_SOCKET
#ifdef CONFIG_CTRL_IFACE_UNIX
#include <sys/un.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#endif /* CONFIG_CTRL_IFACE_UNIX */
#include <sys/uio.h>
#include <sys/time.h>

/**
 * struct wpa_ctrl - Internal structure for control interface library
 *
 * This structure is used by the wpa_supplicant/hostapd control interface
 * library to store internal data. Programs using the library should not touch
 * this data directly. They can only use the pointer to the data structure as
 * an identifier for the control interface connection and use this as one of
 * the arguments for most of the control interface library functions.
 */
struct wpa_ctrl {
        int s;
        struct sockaddr_un local;
        struct sockaddr_un dest;
};

char statusCmdBuf[2048];

#ifdef CONFIG_CTRL_IFACE_UNIX

struct wpa_ctrl * wpa_ctrl_open(const char *ctrl_path);

void wpa_ctrl_close(struct wpa_ctrl *ctrl);

int wpa_ctrl_request(struct wpa_ctrl *ctrl, const char *cmd, size_t cmd_len,
                     char *reply, size_t *reply_len,
                     void (*msg_cb)(char *msg, size_t len));

void wpa_cli_msg_cb(char *msg, size_t len);

int _wpa_ctrl_command(struct wpa_ctrl *ctrl, char *cmd, int print);

char * wpa_cli_get_default_ifname(void);

int wpa_cli_open_connection(const char *ifname);

void wpa_cli_close_connection(void);

int scan_ssid();

int find_status();

#endif /* CONFIG_CTRL_IFACE_UNIX */

#endif /* WPACLI_H */


