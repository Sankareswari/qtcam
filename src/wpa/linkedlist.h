#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

struct ssid_struct
{
    char ssid[50];
    struct ssid_struct *next;
};

bool create_list(char *ssid);

bool add_to_list(char *ssid, bool add_to_end);

void delete_list();

struct ssid_struct * get_ssid_list(void);
