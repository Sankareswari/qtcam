/*
 * wificam.cpp -- wifi network scan.
 * Copyright © 2015  e-con Systems India Pvt. Limited
 *
 * This file is part of Qtcam.
 *
 * Qtcam is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * Qtcam is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Qtcam. If not, see <http://www.gnu.org/licenses/>.
 */
#include <QDebug>
#include <string.h>
#include "wificam.h"

extern "C"{
#include "wpa/wpa_command.h"
#include "wpa/linkedlist.h"
}

WifiCam::WifiCam():m_ssidList(NULL),
                   m_ctrlInterfaceName(NULL){

}

WifiCam::~WifiCam(){

}

/**
 * @brief get default wlan interface name
 * @param NIL
 * @returns true/false
 */
bool WifiCam::getDefaultInterfaceName(){
    if(!m_ctrlInterfaceName){
        // Get the default wireless interface name
        m_ctrlInterfaceName = wpa_cli_get_default_ifname();

        // if interface name [ex:wlan0] is NULL, failure
        if(!m_ctrlInterfaceName)
            return false;
    }

    return true;
}

/**
 * @brief open wifi connection using interface name
 * @param NIL
 * @returns true/false
 */
bool WifiCam::openConnection(){
    if(!m_ctrlInterfaceName)
        return false;

    // opening wifi interface connection
    if(wpa_cli_open_connection(m_ctrlInterfaceName) != 0){
        return false;
    }

    return true;
}

/**
 * @brief scan all APs and extract wifi camera from AP list
 * @param NIL
 * @returns wifi camera list
 */
QStringList WifiCam::extractWifiCamsFromAPList(){
     QStringList wifiCamList;

     // sending scan and scan_results commands.
     // scan results gives list in the format "bssid / frequency / signal level / flags / ssid"
     // if failure to get the above list, return empty list
     if (0 != scan_ssid())
         return wifiCamList;

     //extract ssid names from the format above mentioned
     m_ssidList = get_ssid_list();
     while(m_ssidList != NULL)
     {         
         // extracting wifi cameras from all wifi access points
         if(strstr(m_ssidList->ssid, "Cam") || strstr(m_ssidList->ssid, "cam"))
            wifiCamList.append(m_ssidList->ssid);
         m_ssidList = m_ssidList->next;
     }

     return wifiCamList;
}

/**
 * @brief: close wifi interface connection
 * @param NIL
 * @returns NIL
 */
void WifiCam::closeConnection(){    
    wpa_cli_close_connection();
}

/**
 * @brief: Getting status of selected camera status
 * @param: device name
 * @returns true/false
 */
bool WifiCam::getStatus(QString deviceName){

    // find status of selected camera device
    if(0 != find_status()){
        return false;
    }

    // if "wpa_state is COMPLETED" then we can conclude as wifi camera is connected.
    if(strstr(statusCmdBuf,deviceName.toStdString().c_str()) && strstr(statusCmdBuf, "wpa_state=COMPLETED")){           
        return true;
    }
    else{                
        return false;
    }
}

