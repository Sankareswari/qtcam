#ifndef WIFICAM_H
#define WIFICAM_H

#include <QStringList>

class WifiCam{
public:
    WifiCam();
    ~WifiCam();

    struct ssid_struct *m_ssidList;
    char *m_ctrlInterfaceName;

    // methods
    bool getDefaultInterfaceName();

    bool openConnection();

    QStringList extractWifiCamsFromAPList();

    void closeConnection();

    bool getStatus(QString deviceName);
};

#endif // WIFICAM_H
